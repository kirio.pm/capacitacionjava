/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dao;

import com.helpdesk.capacitacionjava.modelos.Bitacora;
import com.helpdesk.capacitacionjava.util.NewHibernateUtil;
import java.util.List;
import javax.ejb.Stateless;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author David Ramirez
 */

@Stateless
public class BitacoraDaoImpl implements BitacoraDao{
    
    @Override
    public List<Bitacora> listaBitacora() {
       Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        List<Bitacora> listaBitacora=session.createQuery("From Bitacora").list();
        transaction.commit();
        return listaBitacora;
    }
    
    @Override
    public void guardarBitacora(Bitacora bitacora) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.persist(bitacora);
        transaction.commit();
    }

    @Override
    public void actualizarBitacora(Bitacora bitacora) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.saveOrUpdate(bitacora);
        transaction.commit();
    }

    @Override
    public Bitacora buscarBitacora(Integer bita_id) {
       Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        Bitacora bitacoraN = (Bitacora) session.get(Bitacora.class, bita_id);
        transaction.commit();
        return bitacoraN;
    }

    @Override
    public void eliminarBitacora(Bitacora bitacora) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        session.delete(bitacora);
        transaction.commit();
    }

    
    
}
