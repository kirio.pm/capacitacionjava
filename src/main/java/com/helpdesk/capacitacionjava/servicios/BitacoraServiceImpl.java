/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.servicios;

import com.helpdesk.capacitacionjava.dao.BitacoraDao;
import com.helpdesk.capacitacionjava.modelos.Bitacora;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author David Ramirez
 */
@Stateless
public class BitacoraServiceImpl implements BitacoraService{
    @EJB
    BitacoraDao dao;

    @Override
    public List<Bitacora> listaBitacora() {
        return dao.listaBitacora();  
    }
    @Override
    public void guardarBitacora(Bitacora bitacora) {
        dao.guardarBitacora(bitacora);
    }

    @Override
    public void actualizarBitacora(Bitacora bitacora) {
        dao.actualizarBitacora(bitacora);
    }
    
    @Override
    public Bitacora buscarBitacora(Integer bita_id) {
       return dao.buscarBitacora(bita_id);
    }

    @Override
    public void eleminarBitacora(Bitacora bitacora) {
        dao.eliminarBitacora(bitacora);
    }
}
