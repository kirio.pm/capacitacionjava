/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.servicios;

import com.helpdesk.capacitacionjava.dao.RolesDao;
import com.helpdesk.capacitacionjava.modelos.Roles;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author David Ramirez
 */
@Stateless
public class RolesServicesImpl implements RolesService {

    @EJB
    private RolesDao dao;

    @Override
    public List<Roles> listaRoles() {
        return dao.listaRoles();
    }

    @Override
    public Roles obtenerRol(Integer idRol) {
        return dao.obtenerRol(idRol);
    }

}
